# MobX | Simple, scalable state management.

MobX is inspired by reactive programming principles as found in the spreadsheets. It is inspired by MVVM frameworks like MeteorJS tracker, knockout and Vue.js

For more information, you can learn here
https://mobx.js.org/README.html

Today, we will try to integrate the Layout and Response API using MobX.

Let's get started

## Tech Stack

- Javascript ES6
- Axios
- Modal
- Moment (parsing, validating, manipulating and displaying date/time in JavaScript)
- Mobx (user state management)

## Run Locally 

### Step 1
- Run this command `git clone https://gitlab.com/zaiidleader/mobsmobiletest.git`
- Make sure that you are in the root directory of the project, use pwd or cd for (windows, macOS, linux)
- `cd MobileTest`
- For android `npm install` 
- For ios `npm install` and cd ios `arch -x86_64 pod install` for MacBook M1 or `pod install` not M1

### Step 2
- Run `npx react-native run-android` for android
- Run `npx react-native run-ios` for ios

## Screenshots
![Screenshot aplikasi](screenshot.png)

# Challenge
The API response is available and the layout is available.
So, please integrate the api response with the layout, so that the layout matches the api response. thank you

<br /> <br />
