import React, { useRef, useEffect, useState } from 'react'
import {
  SafeAreaView,
  Dimensions,
  StyleSheet,
  Image,
  TouchableOpacity,
  View,
  Text,
  Alert,
  ImageBackground,
  useWindowDimensions,
  ScrollView,
  StatusBar,
  Flatform,
  FlatList
} from 'react-native'

import { allLogo } from '@Assets'
import { toDp } from '@percentageToDP'

import CustomTextInput from '@CustomTextInput'

const { width, height } = Dimensions.get('window')
type Props = {}
const Chat = (props) => {

  const [state, setState] = useState({
    search: '',
    errorSearch: '',
    arrayDummy: [
      {
        user_image: 'https://images.pexels.com/photos/2777898/pexels-photo-2777898.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
        name: 'Darlene Steward',
        message: 'Pls take a look at the images.',
        time: '18.31',
        notif: 5
      },
      {
        user_image: 'https://images.pexels.com/photos/4009626/pexels-photo-4009626.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
        name: 'Daniel Hanson',
        message: 'Pls take a look at the images.',
        time: '18.31',
        notif: 0
      },
      {
        user_image: 'https://images.pexels.com/photos/4939461/pexels-photo-4939461.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
        name: 'Anton Wijaya',
        message: 'Pls take a look at the images.',
        time: '18.31',
        notif: 0
      }
    ],
  })

  const renderItem = ({item, index}) => {
    return (
      <View style={styles.containerItem}>
        <View style={styles.viewUser}>
          <Image source={{uri: item.user_image}} style={styles.userImage} />
          <View style={styles.viewNameDate}>
            <Text style={styles.textName}>{item.name}</Text>
            <Text style={styles.textDate}>{item.message}</Text>
          </View>
        </View>
        <View style={styles.viewTime}>
          <Text style={styles.textTime}>{item.time}</Text>
          {
            item.notif !== 0 &&
            <View style={styles.viewNotif}>
              <Text style={styles.textNotif}>{item.notif}</Text>
            </View>
          }
        </View>
      </View>
    )
  }

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar backgroundColor='white' barStyle="dark-content" />

      <View style={styles.header}>
        <View style={styles.viewHeader}>
          <TouchableOpacity style={styles.touchBack} onPress={() => props.navigation.goBack()}>
            <Image source={allLogo.icBack} style={styles.icBack} />
          </TouchableOpacity>
          <Text style={styles.textTitle}>Chat</Text>
          <View style={styles.viewRight}>
            <Image source={allLogo.icPlus} style={styles.icPlus} />
            <Image source={allLogo.icMoreVertical} style={styles.icMoreVertical} />
          </View>
        </View>
        <View style={styles.viewSearch}>
          <CustomTextInput
            error={state.errorSearch}
            include={false}
            placeholder={'Chat, Friend'}
            value={state.search}
            onChangeText={(search) => {
              setState(state => ({...state, search, errorSearch: search === '' ? '' : ''}))
            }}
            autoCapitalize={'none'}
            height={toDp(40)}
          />
        </View>
      </View>

      <View style={{height: toDp(12)}} />

      <View style={styles.content}>
        <FlatList
          data={state.arrayDummy}
          renderItem={renderItem}
          ListFooterComponent={() => <View style={{height: toDp(24)}} />}
        />
      </View>

    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  content: {
    flex: 1,
    backgroundColor: '#F2F3F3'
  },
  header: {
    width,
    height: 'auto',
    backgroundColor: 'white',
    paddingBottom: toDp(12),
    paddingTop: toDp(12)
  },
  viewHeader: {
    width,
    height: 'auto',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    paddingHorizontal: toDp(12),
  },
  touchBack: {
    width: toDp(80),
    padding: toDp(4)
  },
  icBack: {
    width: toDp(20),
    height: toDp(17.08)
  },
  textTitle: {
    color: '#181725',
    fontSize: toDp(16),
    fontWeight: '600'
  },
  viewRight: {
    width: toDp(80),
    flexDirection: 'row',
    alignItems: 'center'
  },
  icPlus: {
    width: toDp(28),
    height: toDp(28)
  },
  icMoreVertical: {
    width: toDp(32),
    height: toDp(8),
    marginLeft: toDp(12)
  },
  viewSearch: {
    width: '92.5%',
    marginHorizontal: toDp(16),
    marginTop: toDp(24)
  },
  containerItem: {
    width: '94%',
    height: toDp(82),
    backgroundColor: 'white',
    marginHorizontal: toDp(12),
    marginBottom: toDp(12),
    borderRadius: toDp(12),
    marginTop: toDp(16),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  viewUser: {
    flexDirection: 'row',
    marginLeft: toDp(16)
  },
  viewNameDate: {
    justifyContent: 'center',
    marginLeft: toDp(16),
  },
  userImage: {
    width: toDp(48),
    height: toDp(48),
    borderRadius: toDp(24)
  },
  textName: {
    fontSize: toDp(16),
    color: '#363636',
    fontWeight: '600',
  },
  textDate: {
    marginTop: toDp(4),
    fontSize: toDp(14),
    color: '#4F5E7B',
    fontWeight: '400'
  },
  viewTime: {
    width: toDp(48),
    height: toDp(48),
    margin: toDp(12),
    alignItems: 'center'
  },
  textTime: {
    fontSize: toDp(12),
    color: '#333333',
    fontWeight: '400'
  },
  viewNotif: {
    width: toDp(24),
    height: toDp(24),
    borderRadius: toDp(12),
    backgroundColor: '#EB5757',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: toDp(6)
  },
  textNotif: {
    fontSize: toDp(12),
    color: 'white',
    fontWeight: '700'
  }
})

export default Chat
