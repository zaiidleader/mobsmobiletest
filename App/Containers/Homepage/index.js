import React, { useRef, useEffect, useState } from 'react'
import {
  SafeAreaView,
  Dimensions,
  StyleSheet,
  Image,
  TouchableOpacity,
  View,
  Text,
  Alert,
  ImageBackground,
  useWindowDimensions,
  ScrollView,
  StatusBar,
  Flatform
} from 'react-native'

import { allLogo } from '@Assets'
import { toDp } from '@percentageToDP'

import CustomTextInput from '@CustomTextInput'
import NavigatorService from '@NavigatorService'
import Modal from 'react-native-modal'

import Home from './Home'

const { width, height } = Dimensions.get('window')
type Props = {}
const Homepage = (props) => {

  const [state, setState] = useState({
    search: '',
    errorSearch: '',
    modalVisibleMenu: false,
  })

  const renderMenu = () => {
    return (
      <Modal
        onBackdropPress={() => setState(state => ({...state, modalVisibleMenu: false}))}
        isVisible={state.modalVisibleMenu}
        style={styles.leftModal}
        animationIn="slideInLeft"
        animationOut="slideOutLeft"
        coverScreen={false}
      >
        <View style={styles.containerModal}>
          {
            Platform.OS === 'ios' ? <View style={{height: toDp(50), backgroundColor: 'white'}} /> :
            <View style={{height: toDp(26), backgroundColor: 'white'}} />
          }
          <View style={styles.headerModal}>
            <View style={styles.viewUser}>
              <Image source={{uri: 'https://images.pexels.com/photos/4009626/pexels-photo-4009626.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500'}} style={styles.userImage} />
              <View style={styles.viewNameDate}>
                <Text style={styles.textName}>Andrew Jones</Text>
                <Text style={styles.textProfile}>View my profile</Text>
              </View>
            </View>
            <TouchableOpacity style={styles.touchClose} onPress={() => setState(state => ({...state, modalVisibleMenu: false}))}>
              <Image source={allLogo.icClose} style={styles.icClose} />
            </TouchableOpacity>
          </View>
          <TouchableOpacity style={styles.touchEmergency}>
            <Image source={allLogo.icEmergency} style={styles.icEmergency} />
            <Text style={styles.textEmergency}>Emergency Button</Text>
          </TouchableOpacity>
          <View>
            <View style={{height: toDp(8)}} />
            <TouchableOpacity style={styles.touchMenuModal}>
              <Image source={allLogo.icInvoice} style={styles.iconModal} />
              <Text style={styles.textModal}>Invoice</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchMenuModal}>
              <Image source={allLogo.icDelivery} style={styles.iconModal} />
              <Text style={styles.textModal}>Delivery Note</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchMenuModal}>
              <Image source={allLogo.icBuilding} style={styles.iconModal} />
              <Text style={styles.textModal}>Building Management Contacts</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchMenuModal}>
              <Image source={allLogo.icSettings} style={styles.iconModal} />
              <Text style={styles.textModal}>Settings</Text>
            </TouchableOpacity>
          </View>
        </View>

      </Modal>
    )
  }

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar backgroundColor='white' barStyle="dark-content" />
      {renderMenu()}
      <View style={styles.header}>
        <TouchableOpacity style={styles.touchMenu} onPress={() => setState(state => ({...state, modalVisibleMenu: true}))}>
          <Image source={allLogo.icMenu} style={styles.icMenu} />
        </TouchableOpacity>
        <View style={styles.viewSearch}>
          <CustomTextInput
            error={state.errorSearch}
            include={false}
            placeholder={'Friends, events, food, etc..'}
            value={state.search}
            onChangeText={(search) => {
              setState(state => ({...state, search, errorSearch: search === '' ? '' : ''}))
            }}
            autoCapitalize={'none'}
            height={toDp(40)}
          />
        </View>
        <TouchableOpacity style={styles.touchMenu} onPress={() => NavigatorService.navigate('Chat')}>
          <Image source={allLogo.icChat} style={styles.touchMenu} />
        </TouchableOpacity>
        <TouchableOpacity style={[styles.touchMenu, {marginLeft: toDp(12)}]}>
          <Image source={allLogo.icNotification} style={styles.touchMenu} />
        </TouchableOpacity>
      </View>

      <View style={styles.content}>
        <Home />
      </View>

      <View style={styles.footer}>
        <TouchableOpacity style={styles.touchFooter}>
          <Image source={allLogo.icHome} style={styles.touchFooter} />
        </TouchableOpacity>
        <TouchableOpacity style={styles.touchFooter}>
          <Image source={allLogo.icResidents} style={styles.touchFooter} />
        </TouchableOpacity>
        <TouchableOpacity style={styles.touchFooter}>
          <Image source={allLogo.icEvents} style={styles.touchFooter} />
        </TouchableOpacity>
        <TouchableOpacity style={styles.touchFooter}>
          <Image source={allLogo.icSharing} style={styles.touchFooter} />
        </TouchableOpacity>
        <TouchableOpacity style={styles.touchFooter}>
          <Image source={allLogo.icServices} style={styles.touchFooter} />
        </TouchableOpacity>
      </View>

      <TouchableOpacity style={styles.touchFab}>
        <Image source={allLogo.fab} style={styles.fab} />
      </TouchableOpacity>

    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  content: {
    flex: 1,
    backgroundColor: '#F2F3F3'
  },
  header: {
    width,
    height: 'auto',
    padding: toDp(12),
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    alignItems: 'center',
  },
  touchMenu: {
    width: toDp(40),
    height: toDp(40),
    justifyContent: 'center',
    alignItems: 'center',
  },
  icMenu: {
    width: toDp(32),
    height: toDp(26)
  },
  viewSearch: {
    width: toDp(234),
    marginHorizontal: toDp(12)
  },

  footer: {
    width,
    height: toDp(64),
    borderTopColor: '#11111180',
    borderTopWidth: toDp(0.5),
    backgroundColor: 'white',
    flexDirection: 'row'
  },
  touchFooter: {
    flex: 1,
    width: toDp(75),
    height: toDp(71),
  },
  touchFab: {
    position: 'absolute',
    right: toDp(16),
    bottom: toDp(140)
  },
  fab: {
    width: toDp(55),
    height: toDp(55)
  },
  leftModal: {
    width: toDp(347),
    marginLeft: 0,
    marginTop: 0,
    marginBottom: 0,
    backgroundColor: '#FFFFFF',
  },
  containerModal: {
    flex: 1,
  },
  headerModal: {
    flexDirection: 'row',
    marginLeft: toDp(12),
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  viewUser: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  userImage: {
    width: toDp(50),
    height: toDp(50),
    borderRadius: toDp(25)
  },
  viewNameDate: {
    marginLeft: toDp(16),
  },
  textName: {
    fontSize: toDp(18),
    color: '#363636',
    fontWeight: '600',
  },
  textProfile: {
    marginTop: toDp(2),
    fontSize: toDp(14),
    color: '#3DB799',
    fontWeight: '600',
  },
  touchClose: {
    padding: toDp(4),
    marginRight: toDp(8)
  },
  icClose: {
    width: toDp(24),
    height: toDp(24)
  },
  touchEmergency: {
    width: toDp(323),
    height: toDp(44),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FAE6E3',
    flexDirection: 'row',
    borderRadius: toDp(10),
    marginHorizontal: toDp(12),
    marginTop: toDp(32)
  },
  icEmergency: {
    width: toDp(24),
    height: toDp(24)
  },
  textEmergency: {
    fontSize: toDp(16),
    color: '#EB5757',
    fontWeight: '600',
    marginLeft: toDp(8)
  },
  touchMenuModal: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: toDp(12),
    marginTop: toDp(24)
  },
  iconModal: {
    width: toDp(40),
    height: toDp(40)
  },
  textModal: {
    fontSize: toDp(18),
    color: '#363636',
    fontWeight: '600',
    marginLeft: toDp(8)
  }

})

export default Homepage
