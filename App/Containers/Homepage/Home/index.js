import React, { useEffect, useRef, useState } from 'react'
import {
  SafeAreaView,
  Dimensions,
  StyleSheet,
  Image,
  TouchableOpacity,
  View,
  Text,
  FlatList,
  ImageBackground,
  ActivityIndicator,
  Alert,
  ScrollView,
  Linking,
  RefreshControl
} from 'react-native'

import {inject, observer} from 'mobx-react';
import { allLogo } from '@Assets'
import { toDp } from '@percentageToDP'

import Modal from 'react-native-modal'
import NavigatorService from '@NavigatorService'
import LinearGradient from 'react-native-linear-gradient'

//import { getComplaints } from '@Apis'

const { width, height } = Dimensions.get('window')
type Props = {}
const Home = (props) => {

  const refToast = useRef()
  const [state, setState] = useState({
    loading: true,
    modalVisible: false,
    modalVisibleMenu: false,
    arrayDummy: [{
      user_image: 'https://images.pexels.com/photos/2777898/pexels-photo-2777898.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
      name: 'Kezia Elizabeth',
      date: 'Today at 14:03',
      post_image: 'https://images.pexels.com/photos/3643104/pexels-photo-3643104.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
      description: '#Vegan Toast Hacks That Will Literally Change Your Life Forever. We’ve got a slew of toasty ideas that’ll take your morning meals to new heights. Cheers to a healthier daily lifestyle!',
    }, {
      user_image: 'https://images.pexels.com/photos/4009626/pexels-photo-4009626.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
      name: 'Andrew Jones',
      date: 'Today at 14:03',
      post_image: 'https://images.pexels.com/photos/2272825/pexels-photo-2272825.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
      description: '#Vegan Toast Hacks That Will Literally Change Your Life Forever. We’ve got a slew of toasty ideas that’ll take your morning meals to new heights. Cheers to a healthier daily lifestyle!',
    }, {
      user_image: 'https://images.pexels.com/photos/4939461/pexels-photo-4939461.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
      name: 'Kezia Elizabeth',
      date: 'Today at 14:03',
      post_image: 'https://images.pexels.com/photos/1829183/pexels-photo-1829183.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
      description: '#Vegan Toast Hacks That Will Literally Change Your Life Forever. We’ve got a slew of toasty ideas that’ll take your morning meals to new heights. Cheers to a healthier daily lifestyle!',
    }, {
      user_image: 'https://images.pexels.com/photos/11263473/pexels-photo-11263473.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
      name: 'Andrew Jones',
      date: 'Today at 14:03',
      post_image: 'https://images.pexels.com/photos/3075535/pexels-photo-3075535.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
      description: '#Vegan Toast Hacks That Will Literally Change Your Life Forever. We’ve got a slew of toasty ideas that’ll take your morning meals to new heights. Cheers to a healthier daily lifestyle!',
    }],
  })

  useEffect(() => {
    props.reportStore.getReport(callback)
  }, [])

  const callback = (response) => {
    //console.log(response);
  }

  const renderModal = () => {
    return (
      <Modal
        onBackdropPress={() => setState(state => ({...state, modalVisible: false}))}
        isVisible={state.modalVisible}
        style={styles.bottomModal}
      >

        <View style={styles.viewRootModal}>
          <View style={[styles.modalBox, {backgroundColor: '#FFFFFF'}]}>
            <View style={styles.viewCenter}>
              <View style={styles.lineModal} />
              <Text style={styles.titleModal}>Post Filer</Text>
            </View>
            <View style={styles.rowModal}>
              <Text style={styles.textModal}>See your post on</Text>
              <TouchableOpacity style={styles.touchButtonModal}>
                <Text style={styles.textButtonModal}>All Post</Text>
              </TouchableOpacity>
            </View>
            <View style={[styles.rowModal, {marginTop: toDp(20)}]}>
              <Text style={styles.textModal}>Privacy</Text>
              <TouchableOpacity style={styles.touchButtonModal}>
                <Text style={styles.textButtonModal}>Everyone</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>

      </Modal>
    )
  }

  const renderItem = ({item, index}) => {
    return (
      <View style={styles.containerItem}>
        <View style={styles.header}>
          <View style={styles.viewUser}>
            <Image source={{uri: item.user_image}} style={styles.userImage} />
            <View style={styles.viewNameDate}>
              <Text style={styles.textName}>{item.name}</Text>
              <Text style={styles.textDate}>{item.date}</Text>
            </View>
          </View>
          <TouchableOpacity style={styles.touchMore}
            onPress={() => setState(state => ({...state, modalVisible: true}))}
          >
            <Image source={allLogo.icMore} style={styles.icMore} />
          </TouchableOpacity>
        </View>
        <View style={styles.viewContent}>
          <Image source={{uri: item.post_image}} style={styles.postImage} />
          <View style={styles.viewImage}>
            <LinearGradient colors={['#3A3A3A33', 'transparent']} style={styles.gradientTop} />
            <LinearGradient colors={['transparent', '#3A3A3ACC']} style={styles.gradientBottom} />
            <Image source={allLogo.icVideo} style={styles.icVideo} />
          </View>
          <Text style={styles.textDesc}>{item.description}</Text>
          <Image source={allLogo.icGroup} style={styles.icGroup} />
        </View>
        <View style={styles.line} />
        <View style={styles.viewFooter}>
          <TouchableOpacity style={styles.touchRow}>
            <Image source={allLogo.icLike} style={styles.icLike} />
            <Text style={styles.text}>Like</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.touchRow}>
            <Image source={allLogo.icComment} style={styles.icComment} />
            <Text style={styles.text}>Comment</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.touchRow}>
            <Image source={allLogo.icShare} style={styles.icShare} />
            <Text style={styles.text}>Share</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  return (
    <View style={styles.container}>
      {renderModal()}
      <FlatList
        data={state.arrayDummy}
        renderItem={renderItem}
        ListFooterComponent={() => <View style={{height: toDp(24)}} />}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  containerItem: {
    width: '94%',
    height: 'auto',
    backgroundColor: 'white',
    margin: toDp(12),
    borderRadius: toDp(8),
    paddingBottom: toDp(16)
  },
  header: {
    width: '100%',
    height: 'auto',
    flexDirection: 'row',
    alignItems: 'center',
    padding: toDp(16),
    justifyContent: 'space-between'
  },
  viewUser: {
    flexDirection: 'row'
  },
  userImage: {
    width: toDp(50),
    height: toDp(50),
    borderRadius: toDp(25)
  },
  viewNameDate: {
    marginLeft: toDp(16),
  },
  textName: {
    fontSize: toDp(18),
    color: '#363636',
    fontWeight: '600',
  },
  textDate: {
    marginTop: toDp(4),
    fontSize: toDp(14),
    color: '#787878'
  },
  touchMore: {
    width: toDp(24),
    height: toDp(24),
    justifyContent: 'center',
    alignItems: 'center',
  },
  icMore: {
    width: toDp(4),
    height: toDp(20)
  },
  viewContent: {
    marginHorizontal: toDp(16),
  },
  postImage: {
    width: toDp(358),
    height: toDp(358),
    borderRadius: toDp(8)
  },
  viewImage: {
    width: toDp(358),
    height: toDp(358),
    position: 'absolute',
    borderRadius: toDp(8)
  },
  textDesc: {
    marginTop: toDp(16),
  },
  icGroup: {
    marginTop: toDp(16),
    width: toDp(358),
    height: toDp(20),
  },
  line: {
    width: width * 0.83,
    height: toDp(1),
    backgroundColor: '#E2E2E23D',
    marginTop: toDp(16),
    marginLeft: toDp(16)
  },
  icVideo: {
    width: toDp(29.48),
    height: toDp(21.79),
    position: 'absolute',
    right: toDp(16),
    top: toDp(16)
  },
  gradientTop: {
    width: '100%',
    height: toDp(74),
    borderTopLeftRadius: toDp(8),
    borderTopRightRadius: toDp(8)
  },
  gradientBottom: {
    width: '100%',
    height: toDp(74),
    borderBottomLeftRadius: toDp(8),
    borderBottomRightRadius: toDp(8),
    position: 'absolute',
    bottom: 0
  },
  viewFooter: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: toDp(16),
    marginHorizontal: toDp(16),
  },
  touchRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icLike: {
    width: toDp(21.54),
    height: toDp(20.13),
    resizeMode: 'contain'
  },
  icComment: {
    width: toDp(19.49),
    height: toDp(19),
    resizeMode: 'contain'
  },
  icShare: {
    width: toDp(19.49),
    height: toDp(17),
    resizeMode: 'contain'
  },
  text: {
    fontSize: toDp(16),
    marginLeft: toDp(8),
    color: '#787878'
  },
  bottomModal: {
    justifyContent: "flex-end",
    margin: 0,
  },
  viewRootModal: {
    width,
    position: 'absolute',
    bottom: 0
  },
  modalBox: {
    width,
    height: toDp(260),
    backgroundColor: '#FFFFFF',
    borderTopLeftRadius: toDp(16),
    borderTopRightRadius: toDp(16)
  },
  modalRow: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  viewCenter: {
    width: '100%',
    alignItems: 'center',
    marginTop: toDp(8)
  },
  lineModal: {
    width: toDp(48),
    height: toDp(4),
    borderRadius: toDp(2.5),
    backgroundColor: '#C4C4C4',
  },
  titleModal: {
    fontSize: toDp(16),
    color: '#363636',
    fontWeight: '700',
    marginTop: toDp(20)
  },
  rowModal: {
    marginTop: toDp(48),
    marginHorizontal: toDp(24),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  textModal: {
    fontSize: toDp(17),
    color: '#363636',
    fontWeight: '600',
  },
  touchButtonModal: {
    width: toDp(160),
    height: toDp(40),
    borderWidth: toDp(1),
    borderColor: '#3EB89A',
    borderRadius: toDp(8),
    justifyContent: 'center',
    alignItems: 'center'
  },
  textButtonModal: {
    fontSize: toDp(16),
    color: '#3EB89A',
    fontWeight: '700',
  },
})

export default inject('reportStore')(observer(Home));
