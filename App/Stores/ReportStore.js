import {types, flow, onSnapshot, getRoot} from 'mobx-state-tree';

import NavigatorService from '@NavigatorService';
import {allLogo} from '@Assets';
import api from '../Services/Api';

export const ReportStore = types
  .model({
    isLoading: false,
    isLoadingUpdate: false,
  })
  .views(self => ({
    root() {
      return getRoot(self);
    },
    getSession() {
      return getRoot(self).sessionStore;
    },
  }))
  .actions(self => ({
    getReport: flow(function* (callback) {
      try {
        const response = yield api.getReport();
        if (response.status >= 200) {
          callback(response);
        } else {
          console.log('getReport ', response);
        }
      } catch (err) {
        console.log('err', err);
      }
    }),
    afterCreate() {
      onSnapshot(self, () => {
        console.log('afterCreate.snapshot');
      });
    },
  }));
