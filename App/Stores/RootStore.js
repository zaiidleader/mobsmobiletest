import {types, onSnapshot, getSnapshot} from 'mobx-state-tree';
import {ReportStore} from './ReportStore';

export const RootStore = types
  .model('RootStore', {
    identifier: types.optional(types.identifier, 'RootStore'),
    reportStore: types.optional(ReportStore, () => ReportStore.create()),
  })
  .actions(self => ({
    async save() {
      try {
        // const transformedSnapshot = getSnapshot(self);
        // const json = JSON.stringify(transformedSnapshot);
        // await AsyncStorage.setItem('appStatePersistenceKey', json);
      } catch (err) {
        console.warn('unexpected error ' + err);
      }
    },
  }));

export async function setupRootStore() {
  try {
    const rootStore = await RootStore.create();
    return rootStore;
  } catch (err) {
    console.tron.log('setupRootStore ', err.message);
    return null;
  }
}
