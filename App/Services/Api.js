import Axios from 'axios';

const api = Axios.create({
  baseURL: 'https://ayodhya-api.alam-sutera.com/',
  headers: {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2NDgxNjI5MzksImV4cCI6MTY0ODE4MDkzOSwiZGF0YSI6eyJ1c2VyX2lkIjoiODU0ODI4ZjUtZmUzOC00YzMxLTg4M2UtMTk3NDFmNTk3ZTI0IiwicGxhdGZvcm0iOiJtb2JpbGUiLCJpc19hX3Jlc2lkZW50IjpmYWxzZSwidW5pdF9pZCI6bnVsbH19.rcjmfDap1o_-b1ATtTShb7ruLHs2-E6313bx7pXWVgs'
  },
  timeout: 15000,
});

api.interceptors.request.use(
  request => { console.log('REQUEST', request); return request },
  error => { console.log('ERROR REQUEST', error); return Promise.reject(error) },
);

api.interceptors.response.use(
  response => { console.log('RESPONSE', response); return response },
  error => { console.log('ERROR RESPONSE', error); return Promise.reject(error.response) },
);

const getReport = () => api.get('api/complaints?page=1');

export default {
  getReport
};
