import { createAppContainer } from "react-navigation";
import { createStackNavigator } from 'react-navigation-stack'

import Homepage from '../Containers/Homepage'
import Chat from '../Containers/Chat'

const AppNavigator = createStackNavigator(
  {
    Homepage: { screen: Homepage },
    Chat: { screen: Chat },
  },
  {
    headerMode: 'none',
    initialRouteName: 'Homepage',
  }
);

export default createAppContainer(AppNavigator)
