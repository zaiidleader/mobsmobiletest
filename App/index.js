import React, { Component, useEffect } from 'react'
import {Provider} from 'mobx-react';
import AppNavigator from './Navigations/AppNavigator'
import NavigatorService from '@NavigatorService'
import {RootStore, setupRootStore} from './Stores/RootStore';

class MobileTest extends Component {

  state = {
    loadedStore: null,
  };

  async componentDidMount() {
    const rootStore = await setupRootStore();
    if (rootStore) {
      this.rootStore = rootStore;
      this.setState({loadedStore: true});
    }
  }

  render() {
    if (!this.state.loadedStore) {
      return null;
    }
    return (
      <Provider {...this.rootStore}>
        <AppNavigator
          ref={navigatorRef => {
            NavigatorService.setContainer(navigatorRef);
          }}
        />
      </Provider>
    );
  }
}

export default MobileTest
