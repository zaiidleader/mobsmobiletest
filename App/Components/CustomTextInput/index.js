import React, { Component } from 'react'
import {BackHandler, TextInput, Dimensions, StyleSheet, Text, View, Image, Alert, Platform, TouchableOpacity, AsyncStorage, ScrollView} from 'react-native'

import { allLogo } from '@Assets'
import { toDp } from '@percentageToDP'

let { width, height } = Dimensions.get('window')

class CustomTextInput extends Component {

  render() {
    const {
        error,
        maxLength,
        value,
        inputRef,
        onChangeText,
        onSubmitEditing,
        placeholder,
        secureTextEntry,
        onChangeSecure,
        type,
        generate
    } = this.props
    return (
      <View style={styles.viewForm}>
        <View style={[styles.viewText, {backgroundColor: generate ? '#e5e5e5' : '#F4F4F4'} , error !== '' && styles.viewError]}>
          {
            <TouchableOpacity style={styles.touchEye} onPress={() => onChangeSecure()}>
              <Image source={allLogo.icSearch} style={styles.icSearch} />
            </TouchableOpacity>
          }
          <TextInput
            ref={r => inputRef && inputRef(r)}
            onChangeText={text => onChangeText(text)}
            onSubmitEditing={() => onSubmitEditing && onSubmitEditing()}
            maxLength={maxLength}
            autoCapitalize={'none'}
            underlineColorAndroid={'transparent'}
            value={value}
            secureTextEntry={secureTextEntry}
            style={[styles.textInput, {color: '#111111'}]}
            placeholder={placeholder}
            placeholderTextColor={'#787878'}
            {...this.props}
          />
        </View>
        {error !== '' && <Text style={styles.textError}>{error}</Text>}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  viewForm: {
    width: '100%',
    height: 'auto',
  },
  textTitle: {
    fontSize: toDp(14),
    letterSpacing: toDp(0.6)
  },
  viewText: {
    width: '100%',
    height: toDp(40),
    borderRadius: toDp(8),
    backgroundColor: 'white',
    paddingHorizontal: toDp(16),
  },
  textInput: {
    flex: 1,
    fontSize: toDp(12),
    color: 'white',
    marginLeft: Platform.OS === 'android' ? toDp(16) : toDp(22),
  },
  textError: {
    marginTop: toDp(4),
    marginLeft: toDp(2),
    fontSize: toDp(14),
    color: '#F5493C',
  },
  viewError: {
    borderColor: '#F5493C',
    backgroundColor: '#ffe2dd'
  },
  customRow: {
    flexDirection: 'row',
    width: '100%',
    backgroundColor: 'red'
  },
  touchEye: {
    position: 'absolute',
    left: toDp(12),
    bottom: toDp(12)
  },
  icSearch: {
    width: toDp(14.92),
    height: toDp(16),
    tintColor: '#787878',
    resizeMode: 'contain'
  }
})


export default CustomTextInput
